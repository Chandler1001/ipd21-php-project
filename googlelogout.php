<?php
require_once 'googleconfig.php';

try {
    if ($adapter->isConnected()) {
        $adapter->disconnect();
    }
    unset($_SESSION['userProfile']);
    //header("Location: https://libraryproject.ipd21/login");
   header("Location: /login");
    session_destroy();
} catch (Exception $e) {
    echo $e->getMessage();
}
