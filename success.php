<?php
// Include database connection file  
require_once '_setup.php';

$payment_id = $statusMsg = '';
$ordStatus = 'error';
if (strpos($_SERVER['HTTP_HOST'], "ipd21.com") !== false) {
    define('DB_HOST', 'libraryproject.ipd21.com');
    define('DB_USERNAME', 'cp4976_libraryproject');
    define('DB_PASSWORD', 'zqmVMaK3WWeUAWUdRI');
    define('DB_NAME', 'cp4976_libraryproject');
} else {
    define('DB_HOST', 'localhost:3333');
    define('DB_USERNAME', 'ipd21project');
    define('DB_PASSWORD', 'xixilele1001');
    define('DB_NAME', 'ipd21project');
}
// Database configuration   

$db = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
if ($db->connect_errno) {
    printf("Connect failed: %s\n", $db->connect_error);
    exit();
}

// Check whether stripe checkout session is not empty 
if (!empty($_GET['session_id'])) {
    $session_id = $_GET['session_id'];

    // Fetch transaction data from the database if already exists 
    $sql = "SELECT * FROM orders WHERE checkout_session_id = '" . $session_id . "'";
    $result = $db->query($sql);
    if ($result->num_rows > 0) {
        $orderData = $result->fetch_assoc();

        $paymentID = $orderData['id'];
        $transactionID = $orderData['txn_id'];
        $paidAmount = $orderData['paid_amount'];
        $paidCurrency = $orderData['paid_amount_currency'];
        $paymentStatus = $orderData['payment_status'];
        $ordStatus = 'success';
        $statusMsg = 'Your Payment has been Successful!';
        //clearCart();
    } else {
        // Include Stripe PHP library  
        //echo $_SESSION['userProfile'];
        require_once './vendor/stripe/stripe-php/init.php';

        // Set API key 
        \Stripe\Stripe::setApiKey('sk_test_51HA2pUIjlJ1JmMzPfsIcCJT7REmnESO6hLGzYJbWmtL70DCD9cWdgI8lejxiGDRTimxrMoKIRd1r0DthYQtos42H00jS7xVia3');

        // Fetch the Checkout Session to display the JSON result on the success page 
        try {
            $checkout_session = \Stripe\Checkout\Session::retrieve($session_id);
        } catch (Exception $e) {
            $api_error = $e->getMessage();
        }

        if (empty($api_error) && $checkout_session) {
            // Retrieve the details of a PaymentIntent 
            try {
                $intent = \Stripe\PaymentIntent::retrieve($checkout_session->payment_intent);
            } catch (\Stripe\Exception\ApiErrorException $e) {
                $api_error = $e->getMessage();
            }

            // Retrieves the details of customer 
            try {
                // Create the PaymentIntent 
                $customer = \Stripe\Customer::retrieve($checkout_session->customer);
            } catch (\Stripe\Exception\ApiErrorException $e) {
                $api_error = $e->getMessage();
            }

            if (empty($api_error) && $intent) {
                // Check whether the charge is successful 
                if ($intent->status == 'succeeded') {
                    // Customer details 
                    $name = $customer->name;
                    $email = $customer->email;

                    // Transaction details  
                    $transactionID = $intent->id;
                    $paidAmount = $intent->amount;
                    $paidAmount = ($paidAmount / 100);
                    $paidCurrency = $intent->currency;
                    $paymentStatus = $intent->status;

                    $id = intval($_SESSION['userProfile']['id']);
                    
                    if ($_SESSION['userProfile']['paytype'] == "fine") {
                        $paymenttype= 2;
                    } else {
                        $paymenttype = 1;
                    }
                    // Insert transaction data into the database 
                    $sql = "INSERT INTO orders(name,email,item_name,item_number,item_price,item_price_currency,paid_amount,
                            paid_amount_currency,txn_id,payment_status,checkout_session_id,created,modified,userid,paymenttype) 
                            VALUES('" . $name . "','" . $email . "','" . $productName . "','" . $productID . "','" . $productPrice . "','" . $currency . "','" .
                        $paidAmount . "','" . $paidCurrency . "','" . $transactionID . "','" . $paymentStatus . "','" . $session_id . "',NOW(),NOW(),'" . $id . "','" . $paymenttype . "')";
                    $insert = $db->query($sql);
                    $paymentID = $db->insert_id;
                    if ($_SESSION['userProfile']['paytype']=="fine" ) {
                        clearFine($paymentID);
                    }else{
                        clearCart($paymentID);
                    }
                    
                    // If the order is successful 
                    if ($paymentStatus == 'succeeded') {
                        $statusMsg = 'Your Payment has been Successful!';
                    } else {
                        $statusMsg = "Your Payment has failed!";
                    }
                } else {
                    $statusMsg = "Transaction has been failed!";
                }
            } else {
                $statusMsg = "Unable to fetch the transaction details! $api_error";
            }

            $ordStatus = 'success';
        } else {
            $statusMsg = "Transaction has been failed! $api_error";
        }
    }
} else {
    $statusMsg = "Invalid Request!";
}
function clearFine($paymentID)
{
    
    $id = intval($_SESSION['userProfile']['id']);
    
    $fineCount = DB::queryFirstField("SELECT count(*)
                        FROM borrowinfo where  userid = '$id' and  fine >0 ");
    $fineResult = DB::query("SELECT *
                        FROM borrowinfo where  userid = '$id' and  fine >0 ");
    
    foreach ($fineResult as $key => &$item) {
        $bookId=$item["bookid"];
        $count = 1;
        $totalprice = $item["fine"];
        DB::insert('orderhistory', ['userid' => $id, 'bookid' => $bookId, 'count' => $count, 'totalprice' => $totalprice, 'orderid' => $paymentID]);
        $userid = DB::insertId();
        if (!$userid) {
            return "error";
        } 
    }
    DB::update('borrowinfo', ['fine' => 0], "userid=%s", $id);
    $_SESSION['userProfile']['borrowcount'] = $_SESSION['userProfile']['borrowcount'] - $fineCount;
}
function clearCart($paymentID)
{
    foreach ($_SESSION['cart'] as $key => &$item) {
        $id = intval($_SESSION['userProfile']['id']);
        $key = intval($key);
        $count = intval($item["count"]);
        $totalprice = $count * intval($item["price"]);
        DB::insert('orderhistory', ['userid' => $id, 'bookid' => $key, 'count' => $count, 'totalprice' => $totalprice, 'orderid' => $paymentID]);
        $id = DB::insertId();
        if (!$id) {
            return "error";
        } else {
            DB::delete('cart', 'id=%s', $item["cartid"]);
        }
        unset($_SESSION['cart'][$key]);
    }

    //unset($_SESSION['cart']);
    $_SESSION['userProfile']['cartcount'] = 0;
}
?>

<!DOCTYPE html>
<html lang="en-US">

<head>
    <title>Stripe Payment Status - CodexWorld</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.0.1">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/navbar-fixed/">
    <link rel="stylesheet" href="/public/css/styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/080dc797b0.js" crossorigin="anonymous"></script>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- Stylesheet file -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body>
    <div class="container">
        <div class="status">
            <h1 class="<?php echo $ordStatus; ?>"><?php echo $statusMsg; ?></h1>
            <?php if (!empty($paymentID)) { ?>
                <h4>Payment Information</h4>
                <p><b>Reference Number:</b> <?php echo $paymentID; ?></p>
                <p><b>Transaction ID:</b> <?php echo $transactionID; ?></p>
                <p><b>Paid Amount:</b> <?php echo $paidAmount . ' ' . $paidCurrency; ?></p>
                <p><b>Payment Status:</b> <?php echo $paymentStatus; ?></p>

                <h4>Product Information</h4>
                <p><b>Name:</b> <?php echo $productName; ?></p>
                <p><b>Price:</b> <?php echo $paidAmount . ' ' . $currency; ?></p>
            <?php } ?>
        </div>
        <a href="index.php" class="btn-link">Back to Product Page</a>
    </div>
</body>

</html>