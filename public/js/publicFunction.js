 var maxPages;
 var currPageNo=0;
 var productStatus=0;
//  var categoryParentText;
//  var categorySubText;
var categoryText;
    export function fetchAllbook(status,newPageNo) {
        productStatus=status;
        $.get("/saleproducts/fetchpaginated/" + productStatus + "/"+ newPageNo, function (data) {
            if (data) {
                let returnInfo = JSON.parse(data);
                maxPages = returnInfo.maxPages;
                currPageNo = returnInfo.pageNo;
                for (let i = 0; i < maxPages + 2; i++) {
                    var liStr = "";
                    if (i == 0) {
                        liStr = `
                                <li class="page-item" >
                                    <a class="page-link" name="previous" >Previous</a>
                                </li>
                                `;
                    } else if (i == maxPages + 1) {
                        liStr = `
                                <li class="page-item">
                                    <a class="page-link" name="next" >Next</a>
                                </li>
                                `;
                    } else if (i > 3) {
                        liStr = `
                                <li class="page-item">
                                    <a class="page-link" name="${i}" style="display:none">${i}</a>
                                </li>
                                `;
                    } else {
                        liStr = `
                                <li class="page-item">
                                    <a class="page-link" name="${i}">${i}</a>
                                </li>
                                `;
                    }
                    $('.pagination').append(liStr);
                }
                $(`a[name =${currPageNo}]`).css("background-color", "grey");
                $(`a[name =${currPageNo}]`).css("color", "white");
                document.querySelectorAll(".page-link").forEach((element) => {
                    element.addEventListener('click', (e) => fetchBookByPage(e));
                });
            }
        })
        if (productStatus == 1) {
            $.get("/saleproducts/fetchpagecontent/" + productStatus + "/" + newPageNo, function (list) {
                let returnInfo = JSON.parse(list);

                appendFileLoad(returnInfo, "#saleDatas").then((value) => {
                    prepareListenner();
                });
            })
        } else if (productStatus == 2) {
            $.get("/saleproducts/fetchpagecontent/" + productStatus + "/" + newPageNo, function (list) {
                let returnInfo = JSON.parse(list);

                appendFileLoad(returnInfo, "#borrowsDatas").then((value) => {
                    prepareListenner();
                });
            })
        }


      
    }
    export function fetchBookByPage(e, noHistory = false) {
        //$("#saleDatas").empty();

        if (e) {
            let pageNo = e.target.name;
            if ((pageNo != "previous") && (pageNo != "next")) {
                currPageNo = pageNo;

            } else if ((pageNo == "previous") && currPageNo > 1) {
                if ($(`a[name =${parseInt(currPageNo) - 1}]`).css("display") == "none") {
                    $(`a[name =${parseInt(currPageNo) - 1}]`).css("display", "block");
                    $(`a[name =${parseInt(currPageNo) + 2}]`).css("display", "none");
                }
                currPageNo = parseInt(currPageNo) - 1;
            } else if ((pageNo == "next") && currPageNo < maxPages) {
                if ($(`a[name =${parseInt(currPageNo) + 1}]`).css("display") == "none") {
                    $(`a[name =${parseInt(currPageNo) + 1}]`).css("display", "block");
                    $(`a[name =${parseInt(currPageNo) - 2}]`).css("display", "none");
                }
                currPageNo = parseInt(currPageNo) + 1;
            }
        }

        for (let i = 0; i < maxPages + 1; i++) {
            if (currPageNo != i) {
                $(`a[name =${i}]`).css("background-color", "white");
                $(`a[name =${i}]`).css("color", "grey");
            }
        }
        $(`a[name =${currPageNo}]`).css("background-color", "grey");
        $(`a[name =${currPageNo}]`).css("color", "white");

        $.get("/saleproducts/fetchpagecontent/" + productStatus + "/"  + currPageNo, function (list) {
        
            let returnInfo = '';
            returnInfo = JSON.parse(list);

         

            if (productStatus == 1) {
                appendFileLoad(returnInfo, "#saleDatas").then((value) => {
                    prepareListenner();
                });
            } else if (productStatus == 2) {
                appendFileLoad(returnInfo, "#borrowsDatas").then((value) => {
                    prepareListenner();
                });
            }
        })
    }
    export function fetchBookByCategoryPage(e, noHistory = false) {
        if (e) {
            let pageNo = e.target.name;
            if ((pageNo != "previous") && (pageNo != "next")) {
                currPageNo = pageNo;
            } else if ((pageNo == "previous") && currPageNo > 1) {
                if ($(`a[name =${parseInt(currPageNo) - 1}]`).css("display") == "none") {
                    $(`a[name =${parseInt(currPageNo) - 1}]`).css("display", "block");
                    $(`a[name =${parseInt(currPageNo) + 2}]`).css("display", "none");
                }
                currPageNo = parseInt(currPageNo) - 1;
            } else if ((pageNo == "next") && currPageNo < maxPages) {
                if ($(`a[name =${parseInt(currPageNo) + 1}]`).css("display") == "none") {
                    $(`a[name =${parseInt(currPageNo) + 1}]`).css("display", "block");
                    $(`a[name =${parseInt(currPageNo) - 2}]`).css("display", "none");
                }
                currPageNo = parseInt(currPageNo) + 1;
            }
        }

        for (let i = 0; i < maxPages + 1; i++) {
            if (currPageNo != i) {
                $(`a[name =${i}]`).css("background-color", "white");
                $(`a[name =${i}]`).css("color", "grey");
            }
        }
        $(`a[name =${currPageNo}]`).css("background-color", "grey");
        $(`a[name =${currPageNo}]`).css("color", "white");

        $.get("/saleproducts/fetchbyid/" + productStatus + "/" + currPageNo + "/" + categoryText, function (list) {
            // if (noHistory == false) {
            //     history.pushState({ page: currPageNo }, '', '/saleproducts/fetchpagecontent/' + currPageNo);
            // }
            //let returnInfo='';
            let returnInfo = '';
            returnInfo = JSON.parse(list);

            if (productStatus == 1) {
                appendFileLoad(returnInfo.booklist, "#saleDatas").then((value) => {
                    prepareListenner();
                });
            } else if (productStatus == 2) {
                appendFileLoad(returnInfo.booklist, "#borrowsDatas").then((value) => {
                    prepareListenner();
                });
            }

            
        })
    }
    // window.addEventListener('popstate', (event) => {
    //     // console.log("location: " + document.location + ", state: " + JSON.stringify(event.state));
    //     let url = window.location.href.split("/");
    //     currPageNo=url[url.length-1]
    //     fetchBookByPage(null, true);
    // });
    export function fetchBookByCategory (status,newPageNo, category) {
        categoryText=category;
        productStatus = status;
        $.get("/saleproducts/fetchbyid/" + productStatus + "/" + newPageNo + "/" + categoryText, function (data) {
            if (data) {
                let returnInfo = JSON.parse(data);
                if (returnInfo.booklist) {
                    maxPages = returnInfo.maxPages;
                    currPageNo = newPageNo;
                    for (let i = 0; i < maxPages + 2; i++) {
                        var liStr = "";
                        if (i == 0) {
                            liStr = `
                                    <li class="page-item" >
                                        <a class="page-link" name="previous" >Previous</a>
                                    </li>
                                    `;
                        } else if (i == maxPages + 1) {
                            liStr = `
                                    <li class="page-item">
                                        <a class="page-link" name="next" >Next</a>
                                    </li>
                                    `;
                        } else if (i > 3) {
                            liStr = `
                                <li class="page-item">
                                    <a class="page-link" name="${i}" style="display:none">${i}</a>
                                </li>
                                `;
                        } else {
                            liStr = `
                                    <li class="page-item">
                                        <a class="page-link" name="${i}">${i}</a>
                                    </li>
                                    `;
                        }
                        $('.pagination').append(liStr);
                    }
                    $(`a[name =${currPageNo}]`).css("background-color", "grey");
                    $(`a[name =${currPageNo}]`).css("color", "white");

                    document.querySelectorAll(".page-link").forEach((element) => {
                        element.addEventListener('click', (e) => fetchBookByCategoryPage(e));
                    });
                    if (productStatus == 1) {
                        appendFileLoad(returnInfo.booklist, "#saleDatas").then((value) => {
                            console.log(value);
                            prepareListenner();
                        });
                    } else if (productStatus == 2) {
                        appendFileLoad(returnInfo.booklist, "#borrowsDatas").then((value) => {
                            console.log(value);
                            prepareListenner();
                        });
                    }
                 
                } 
                // else if (returnInfo.statusCode == 400) {
                //     $('#cart-error-body').html(returnInfo.response);
                //     $('#cart-error').modal('show');
                // }
            }
        })
        // $.get("/saleproducts/fetchpagecontent/" + newPageNo, function (list) {
        //     let returnInfo = JSON.parse(list);
        //     appendFileLoad(returnInfo, "#saleDatas").then((value) => {
        //         prepareListenner();
        //     });
        // })
    }

    export function appendFileLoad (files, listName) {
        return new Promise(resolve => {

            $.get("/userprivilige", function (data) {
                let res = JSON.parse(data);
                let add, value, numberItem ;////img.price Price value numberItem
                if (productStatus == 1) {
                    document.getElementById("saleDatas").innerHTML = "";
                    add = "ADD TO CART";
                    value='Price';
                    //numberItem = img.price
                } else if (productStatus == 2) {
                    document.getElementById("borrowsDatas").innerHTML = "";
                    add = "Add to my lend list";
                    value='Count';
                    //numberItem
                }
                if (res.statusCode == "200") {
                    let privilige = JSON.parse(res.response);
                    if (privilige == 1) {
                        if (files.length > 0) {

                            files.forEach(function (img) {
                                if (productStatus == 1) {
                                    numberItem = img.price
                                } else if (productStatus == 2) {
                                    numberItem = img.countnumber
                                }
                                let image64B = img.picture;
                                let url = 'data:image/png;base64,' + image64B;
                                let liStr = `
                                    <li class="list-group-item col-sm-6 border-0" style="height:300px">
                                        <div class="media align-items-lg-center flex-column flex-lg-row border shadow p-3 h-100 position-relative">
                                            <div class="media-body h-100 w-50 position-relative">
                                                <h5 class="mt-0 font-weight-bold mb-2 book-item-name">${img.name}</h5>
                                                <p class="font-italic text-muted mb-0 small book-item-author">${img.author} </p>
                                                <p class="font-italic text-muted mb-0 small">${img.description} </p>
                                                <div class="d-flex align-items-center justify-content-between mt-1">
                                                    <span class="font-weight-bold">${value}</span><h6 class=" my-2 book-item-price" id="${img.id}">${numberItem}</h6>
                                                </div>
                                                <button class="btn btn-primary add-item-button" name="${img.id}" type="button">${add}</button>
                                            </div>
                                            <button type="button" name="${img.id}" class="close position-absolute close-button close-button-book" aria-label="Close">
                                                    <span aria-hidden="true" >&times;</span>
                                            </button>
                                            <div class="position-absolute edit-button"><a href="/bookload?id=${img.id}" class="text-right">Edit</a></div>
                                            <div class="h-75 w-50 row ml-3 p-0 position-relative">
                                                <img src="${url}" class="rounded w-100  align-self-center mx-auto" alt="Generic placeholder image ">
                                            </div> 
                                        </div>
                                    </li> 
                                `;
                                $(listName).append(liStr);
                            });
                            document.querySelectorAll(".close-button-book").forEach((element) => {
                                element.addEventListener('click', (e) => deleteBook(e));
                            });
                            resolve('Success!');
                        }
                    } else {
                        if (files.length > 0) {
                            files.forEach(function (img) {
                                if (productStatus == 1) {
                                    numberItem = img.price
                                } else if (productStatus == 2) {
                                    numberItem = img.countnumber
                                }
                                let image64B = img.picture;
                                let url = 'data:image/png;base64,' + image64B;
                                let liStr = `
                                        <li class="list-group-item col-lg-6 border-0" style="height:300px">
                                            <div class="media align-items-lg-center flex-column flex-lg-row border shadow p-3 h-100 position-relative">
                                                <div class="media-body h-100 w-50 position-relative">
                                                    <h5 class="mt-0 font-weight-bold mb-2 book-item-name">${img.name}</h5>
                                                    <p class="font-italic text-muted mb-0 small book-item-author">${img.author} </p>
                                                    <p class="font-italic text-muted mb-0 small">${img.description} </p>
                                                    <div class="d-flex align-items-center justify-content-between mt-1">
                                                        <span class="font-weight-bold">${value}</span><h6 class=" my-2 book-item-price" id="${img.id}">${numberItem}</h6>
                                                    </div>
                                                    <button class="btn btn-primary add-item-button" name="${img.id}" type="button">${add}</button>
                                                </div>
                                                <div class="w-50 h-75  row ml-3 p-0 position-relative">
                                                    <img src="${url}" class="rounded  align-self-center mx-auto" alt="Generic placeholder image ">
                                                </div> 
                                            </div>
                                        </li> 
                                    `;
                                $(listName).append(liStr);
                            });
                            resolve('Success!');
                        } 
                    }
                }
            });


        })
    }



    export function deleteBook(event) {
        if (event.currentTarget.name) {
            let id = event.currentTarget.name;

            $.get("/bookdelete/" + id, function (data) {
                let returnInfo = JSON.parse(data);
                if (returnInfo.statusCode == 200) {
                    event.target.parentElement.parentElement.parentElement.remove();
                    $('#cart-added-body').html(returnInfo.response);
                    $('#cart-added').modal('show');
                }
            });
        }
    }

    // window.onunload = function (e) {//页面刷新时先执行onbeforeunload，然后onunload，最后onload。 页面关闭时只执行onunload
    //     //let event = e || window.event;

    //     fetch('/cart/insertsession').then(response => response.json())
    //         .then(data =>
    //             console.log(data)
    //     );
    // }
    export function prepareListenner() {
        let cartBtnEvent = document.querySelectorAll('.add-item-button');
        cartBtnEvent.forEach(element => {
            element.addEventListener('click', cartClicked)
        });
    }
    export function cartClicked(event) {

        let button = event.target;
        let bookid = button.name;
        let shopItem = button.parentElement
        let name = shopItem.getElementsByClassName('book-item-name')[0].innerText
        let price = shopItem.getElementsByClassName('book-item-price')[0].innerText
        let author = shopItem.getElementsByClassName('book-item-author')[0].innerText
        //let url ="/cart/" + bookid + "/" + name + "/" + price + "/" + author;
    
        const data = {
            bookid: bookid,
            name: name,
            price: price,
            author: author,
            count: 1
        };
        if (productStatus == 1) {
            fetch('/cart', {
                method: 'POST', // or 'PUT'
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data),
            })
                .then(
                    // response => response.json()
                    function (response) {
                        if (response.status === 400 ) {
                            //let text=response.json();
                            throw Error("400 - This item is already added in your cart. Please kindly check your cart.");
                            //reject(response.json());                        
                        } else if (response.status === 200) {
                            return response.json();
                        }
                    }
                )
                .then(result => {
                        if (result.statusCode = 200) {
                            $('#cart-added-body').html(result.response);
                            $('#cart-added').modal('show');
                            fetch("/cart/add").then(
                                response => response.json().then(function (data) {
                                    $('#lblCartCount').html(data.response);
                                }
                                ))

                            // setTimeout(function () {
                            //     $('#cart-added').modal('hide');
                            // }, 800);
                        }
                })
                .catch((error) => {
                    console.log(error);
                    $('#cart-error-body').html(error);
                    $('#cart-error').modal('show');
                    // setTimeout(function () {
                    //     $('#cart-error').modal('hide');
                    // }, 1000);
                });

        } else if (productStatus == 2) {
            if ($('#' + bookid).html() == 0) {
                $(".modal-header").addClass('bg-danger');
                $('#borrow-error-body').html("The book is not avaliable");
                $('#borrow-error').modal('show');
                // setTimeout(function () {
                //     $('.modal').modal('hide');
                // }, 600);
            } else { 
                $('#' + bookid).html($('#' + bookid).html() - 1);
                fetch('/borrow', {
                    method: 'POST', // or 'PUT'
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(data),
                })
                    .then(response => response.json())
                    .then(result => {
                        if (result.statusCode != 200) {
                            $('#borrow-error-body').html(result.response);
                           // $('#borrow-error').modal('show');
                            // setTimeout(function () {
                            //     $('#borrow-error').modal('hide');
                            // }, 800);
                        } else
                            if (result.statusCode = 200) {
                                $('#lblBorrowCount').html(result.response);
                                // $('#borrow-added-body').html(result.response);
                                $('#borrow-added').modal('show');
                                // fetch("/cart/add").then(
                                //     response => response.json().then(function (data) {
                                //         $('#lblCartCount').html(data.response);
                                //     }
                                //     ))
                                //     .catch(function (err) {
                                //         console.log('Fetch Error :-S', err);
                                //     });

                                // setTimeout(function () {
                                //     $('#cart-added').modal('hide');
                                // }, 800);
                            }
                    })
                    .catch((error) => {
                        console.error('Error:', error);
                    });

            }
            
        }
 
    }
