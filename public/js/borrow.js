
    "use strict";
    import {fetchAllbook, fetchBookByCategory} from "./publicFunction.js"
    var currPageNo;
    var categoryText;
    // var categorySubText;

    var urlArr = window.location.pathname.split("/");
    // urlArr.split(",");


    if (urlArr[urlArr.length - 1] == 'referenceall') {
        currPageNo = 0;
        fetchAllbook(2, 1);
    }
    else if (urlArr[urlArr.length - 1].split(",")[0] !='nulltext') {
        currPageNo = 0;
        categoryText = urlArr[urlArr.length - 1].split(",")[0];
        fetchBookByCategory(2,1, categoryText);
    } else if (urlArr[urlArr.length - 1].split(",")[0] == 'nulltext'){
        categoryText = urlArr[urlArr.length - 1].split(",")[1];
        fetchBookByCategory(2,1, categoryText);
    } 
