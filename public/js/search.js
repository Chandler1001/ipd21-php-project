$(document).ready(function () {
    
    $('#category-input').on('paste change input keyup', function () {
        var value = $('#category-input').val();
        $("#categoryTaken").load("/iscategoryTaken/" + value);
        $("#category-list").css("display", "block")
        if (value.trim()){
            $.get("/category/getallcatory/" + value.trim(), function (data) {
                let returnInfo = JSON.parse(data);
                searchList = returnInfo.response;
                $("#category-list").empty();
                searchList.forEach(element => {
                    var template = '';
                    template = `
                    <li class="list-group-item search-catorgary" name="${element.id}">${element.name}</li>
                `
                    $("#category-list").append(template);
                  
                });
            });
        }else{
            $("#category-list").css("display", "none")
        }
       
    });
   
    $(document).on("click", ".search-catorgary", function (e) {
        let text = e.target.innerHTML ;
        $('#category-input').val(text);
        $("#category-list").css("display", "none");
    });
});