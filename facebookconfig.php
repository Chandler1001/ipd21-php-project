<?php
require_once 'vendor/autoload.php';

$config = [
    //Location where to redirect users once they authenticate with Facebook
    //For this example we choose to come back to this same script
    "callback" => "https://libraryproject.ipd21.com/facebook.php",//

    //Facebook application credentials
    'keys' => [
        'id'     => '310999100041604', //Required: your Facebook application id
        'secret' => 'a7531eae7db23c1c30debe18672c4f27'  //Required: your Facebook application secret 
    ]
];

$adapter = new Hybridauth\Provider\Facebook($config);
try {
    $adapter->authenticate();
    //$userProfile = $adapter->getUserProfile();
    $_SESSION['userProfile'] = $adapter->getUserProfile();
    // $displayName = $userProfile->displayName;
    // $_SESSION['user'] = $displayName;
    header("Location: /home");
} catch (Exception $e) {
    echo $e->getMessage();
}
