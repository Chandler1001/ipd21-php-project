<?php
require_once '_setup.php';

$app->get('/internalerror', function ($request, $response, $args) {
    return $this->view->render($response, 'error_internal.html.twig');
});

$app->get('/', function ($request, $response, $args) {
    return $this->view->render($response, 'home.html.twig');
});

$app->get('/home', function ($request, $response, $args) {
    if (isset($_SESSION['userProfile']['name'])) {
        $name = $_SESSION['userProfile']['name']; //privilige
        if (isset($_SESSION['userProfile']['privilige'])) {
            $privilige = $_SESSION['userProfile']['privilige'];
        }
    }
    if (isset($privilige)) {
        $result = DB::query("SELECT id FROM category");
        return $this->view->render(
            $response,
            'home.html.twig',
            ["user" => $name, "privilige" => $privilige, "result" => $result]
        );
    }else{
        return $this->view->render(
            $response,
            'home.html.twig'
        );
    }
  
});
$app->get('/bookdelete/{id}', function ($request, $response, $args) {
    if ($args['id']) {
        $id = $args['id'];
        DB::delete('books', 'id=%s', $id);
        $statusCode = "200";
        $result="Delete successful";
        exit(json_encode(array("statusCode" => $statusCode, "response" => $result)));
    }
});
$app->get('/policy', function ($request, $response, $args) {
    return $this->view->render(
        $response,
        'policy.html'
    );
});
$app->get('/bookload', function ($request, $response, $args) {
    $queryParams = $request->getQueryParams();
    if (isset($queryParams['id'])) {
        $id= $queryParams['id'];
        $bookItem = DB::queryFirstRow("SELECT * FROM books where id=%s", $id); //
        $categoryName = DB::queryFirstRow("SELECT name FROM category where id=%s", $bookItem['categoryid'])['name'];
        $bookItem['categoryName']= $categoryName;
        $bookItem['picture'] = base64_encode($bookItem['image']);
        return $this->view->render(
            $response,
            'bookload.html.twig',
            ["bookItem" => $bookItem]
        );
    }else{
        return $this->view->render(
            $response,
            'bookload.html.twig'
        );
    }
});
$app->get('/manageaccounts', function ($request, $response, $args) {
    $id = $_SESSION['userProfile']['id'];

    $list = DB::query("SELECT id,email,phonenumber,name,imageMimeType from users where privilige='2'");

    return $this->view->render(
        $response, 'accountlist.html.twig',["list" => $list]
    );
}); //
$app->get('/manageaccounts/[{accountid}]', function ($request, $response, $args) {
    $id = $args['accountid'];
    $list = DB::query("SELECT borrowinfo.id, books.name,books.author,books.price,books.isbn,books.image,
                            borrowinfo.borrowdate , borrowinfo.returndate
                            FROM borrowinfo,books where books.id = borrowinfo.bookid and borrowinfo.userid = '$id'"); // count(*) as countnum
                            // and borrowinfo.returndate is NULL     
    if (isset($list)  && sizeof($list)>0) {
        foreach ($list as $key => $value) {
            $newArrData[$key] =  $list[$key];
            $newArrData[$key]['picture'] = base64_encode($list[$key]['image']);
            $newArrData[$key]['image'] = "";
        }

        return $this->view->render(
            $response,
            'accountitem.html.twig',
            ["list" => $newArrData]
        );
     }else{
        return $this->view->render(
            $response,
            'accountitem.html.twig'
        );
     }
 
});
$app->get('/manageaccountitem', function ($request, $response, $args) {
    $queryParams = $request->getQueryParams();
    $_SESSION['userProfile']['paytype'] = $queryParams['type'];
    $response = $response->withStatus(200);
    $response->getBody()->write(json_encode(array("code" => "200")));
    return $response;
});
$app->get('/manageaccountitem/{bookinfoid}/{dateformat}', function ($request, $response, $args) {
    $bookinfoid = $args['bookinfoid'];
    $dateformat = $args['dateformat']; // count(*) as countnum
    $borrowdate = DB::queryFirstField("SELECT borrowdate FROM borrowinfo WHERE id='$bookinfoid' and 
            returndate IS NULL ORDER BY ID LIMIT 1");
    if ($borrowdate > $dateformat) {
        // $statusCode = "400";
        // $response = "Return date can not less than borrow. Please check!";
        // exit(json_encode(array("statusCode" => $statusCode, "response" => $response)));

        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode(array("error" => "dateError", "errorText" => "Return date can not less than borrow. Please check!")));
        return $response;
    }
    $day1= strtotime($dateformat);
    $day2 = strtotime($borrowdate);
    $diff =  ceil(($day1-$day2)/ 86400);
    $price=0;
    if ($diff>10) {
        $price= ($diff-10)*0.5;
    }
    $bookid = DB::queryFirstField("select bookid from borrowinfo  WHERE ID = '$bookinfoid'");
    $true1 = DB::query("UPDATE books SET countnumber = countnumber + 1 where id = '$bookid'");
    $true2 = DB::query("UPDATE borrowinfo SET returndate='$dateformat',fine='$price' WHERE ID = '$bookinfoid'");
    if ($true2 && $true1) {
        $statusCode = "200";
        $response = "Book item set to return status successful.";
        exit(json_encode(array("statusCode" => $statusCode, "response" => $response)));
    }else{
        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode(array("error" => "returnError", "errorText" => "System Error. Administrator will check!")));
        return $response;
    }
});
$app->post('/bookload', function ($request, $response, $args) {
    $queryParams = $request->getQueryParams();
    $author = $request->getParam('author');
    $countnumber = $request->getParam('countnumber');
    $categoryname = $request->getParam('category');
    $categoryRow = DB::queryFirstRow("SELECT id FROM category Where name='$categoryname'");
    $category = intval($categoryRow['id']);
    $bookname = $request->getParam('bookname');
    $price = $request->getParam('price');
    $isbn = $request->getParam('isbn');
    if ($_FILES['photo']['tmp_name']) {
        $file = file_get_contents($_FILES['photo']['tmp_name']);
    }
    $description =  $request->getParam('description');
    $status =  $request->getParam('status');
    if (isset($queryParams['id'])) {
        $id = $queryParams['id'];
        $currDate= date("Y-m-d");
        $true1 = DB::query("UPDATE books SET categoryid='$category',
                                            name='$bookname',
                                            author='$author',
                                            price='$price',
                                            instoredate='$currDate',
                                            isbn='$isbn',
                                            countnumber='$countnumber',
                                            description='$description',
                                            status='$status'                                     
                            WHERE id = '$id'");
        if ($true1) {
            if (isset($file)) {
                DB::update('books', ['image' => $file], "id=%s", $id);
            }
            return $this->view->render($response, 'home.html.twig');
        }
     
                                    
    }else{
        DB::insert('books', [
            'categoryid' => intval($category), 'name' => $bookname,
            'author' => $author, 'price' => $price,
            'countnumber' => $countnumber,
            'instoredate' => date("Y-m-d"), 'image' => $file,
            'isbn' => $isbn, 'status' => $status,
            'description' => $description
        ]);
        $id = DB::insertId();
        return $this->view->render($response, 'home.html.twig');
    }
});
$app->get('/saleproducts/{status}/[{reference},{category}]', function ($request, $response, $args) {
    $reference= $args['reference'];
    $category = $args['category'];
    $status = $args['status'];
    if ($status==1) {
        return $this->view->render(
            $response,
            'saleproducts.html.twig',
            ["reference" => $reference, "category" => $category]
        );
    } else   if ($status == 2) {
        return $this->view->render(
            $response,
            'borrowproducts.html.twig',
            ["reference" => $reference, "category" => $category]
        );
    }
  
});



$bookPerPage=4;
$app->get('/saleproducts/fetchpaginated/{status}/{pageNo:[0-9]+}', function ($request, $response, $args) {
   // $result = DB::query("SELECT * FROM books Where status='1'");
    global $bookPerPage;
    $status = $args['status'];
    $pageNo = $args['pageNo'] ?? 1;
    $saleBookCount = DB::queryFirstField("SELECT COUNT(*) AS COUNT FROM books Where status='$status'");
    $maxPages = ceil($saleBookCount / $bookPerPage);
    exit(json_encode(array("maxPages" => $maxPages, "pageNo" => $pageNo)));
    //return json_encode($newArrData);
});

$app->get('/saleproducts/fetchpagecontent/{status}/{pageNo:[0-9]+}', function ($request, $response, $args) {
    global $bookPerPage;
    $newArrData=array();
    $status = $args['status'];
    $pageNo = $args['pageNo'] ?? 1;
    $booklist = DB::query("SELECT * from books  Where status='$status' ORDER BY id DESC LIMIT %d OFFSET %d",
             $bookPerPage, ($pageNo - 1) * $bookPerPage);
    foreach ($booklist as $key => $value) {
        $newArrData[$key] =  $booklist[$key];
        $newArrData[$key]['picture'] = base64_encode($booklist[$key]['image']);
        $newArrData[$key]['image'] = "";
    }
    return json_encode($newArrData);
});


$app->get('/saleproducts/fetchbyid/{status}/{pageNo:[0-9]+}/{categoryname}', function ($request, $response, $args) {
    global $bookPerPage;
    $pageNo = $args['pageNo'] ?? 1;
    $status = $args['status'];
    $categoryname = $args["categoryname"];
    if (ctype_digit($categoryname)) {
        //$saleBookCount = sizeof($booklist);
        $saleBookCount = DB::queryFirstField("SELECT COUNT(*) FROM books Where status='$status' and categoryid in(SELECT id FROM category Where parentid='$categoryname')");
        $booklist = DB::query(
            "SELECT * from books  Where categoryid in(SELECT id FROM category Where parentid='$categoryname') and  status='$status' ORDER BY id DESC LIMIT %d OFFSET %d",
            $bookPerPage,
            ($pageNo - 1) * $bookPerPage
        );
    } else {
        $categoryRow = DB::queryFirstRow("SELECT id FROM category Where name='$categoryname'");
        if ($categoryRow) {
        $category = $categoryRow['id'];
        
            $saleBookCount = DB::queryFirstField("SELECT COUNT(*)  FROM books Where categoryid = '$category' and status='$status'");
            $booklist = DB::query(
                "SELECT * FROM books Where status='$status' and categoryid = '$category' ORDER BY id DESC LIMIT %d OFFSET %d",
                $bookPerPage,
                ($pageNo - 1) * $bookPerPage
            );
        }
    }
    //$size= count($booklist);
    if (isset($booklist)&& count($booklist)>0) {
        $maxPages = ceil($saleBookCount / $bookPerPage);
        foreach ($booklist as $key => $value) {
            $newArrData[$key] =  $booklist[$key];
            $newArrData[$key]['picture'] = base64_encode($booklist[$key]['image']);
            $newArrData[$key]['image'] = "";
        }
        //return json_encode($newArrData);
        exit(json_encode(array("booklist" => $newArrData,"maxPages" => $maxPages)));
    } else {
        // $statusCode = "400";
        // $response = "Category or book not exist in database";
        // exit(json_encode(array("statusCode" => $statusCode, "response" => $response)));

        $response = $response->withStatus(400);//
        $response->getBody()->write(json_encode(array("error" => "categoryError", "errorText" => "Category or book not exist in database!")));
        return $response;
    }
});

$app->get('/iscategoryTaken/[{category}]', function ($request, $response, $args) {
    $category = isset($args['category']) ? $args['category'] : "";
    $record = DB::queryFirstRow("SELECT id FROM category WHERE name='$category'");
    if (!$record) {
        return $response->write("Category not valid in database.");
    } else {
        return $response->write("");
    }
});
// $app->get('/saleproducts/fetchbyid/{categoryname}', function ($request, $response, $args) {
//     $categoryname = $args["categoryname"];
//     if (ctype_digit($categoryname)) {
//         $result = DB::query("SELECT * FROM books Where categoryid in(SELECT id FROM category Where parentid='$categoryname')");

//     }else{
//         $categoryRow = DB::queryFirstRow("SELECT id FROM category Where name='$categoryname'");
//         $category = $categoryRow['id'];
//         $result = DB::query("SELECT * FROM books Where status='1' and categoryid = '$category'");
//         //$result = DB::query("SELECT * FROM category where name like '%$searchTxt%'");

//     }
//     if ($result) {
//         foreach ($result as $key => $value) {
//             $newArrData[$key] =  $result[$key];
//             $newArrData[$key]['picture'] = base64_encode($result[$key]['image']);
//             $newArrData[$key]['image'] = "";
//         }
//         return json_encode($newArrData);
//     }
// });


$app->get('/about', function ($request, $response, $args) {
    //  $name = $_SESSION['user'];
    return $this->view->render(
        $response,
        'about.html.twig'
    );
});


$app->get('/saleproducts/{status}/referenceall', function ($request, $response, $args) {
    //  $name = $_SESSION['user'];
    $status = $args['status'];
    if ($status==1) {
        return $this->view->render(
            $response,
            'saleproducts.html.twig'
        );
    }else{
        return $this->view->render(
            $response,
            'borrowproducts.html.twig'
        );
    }
    
});
$app->get('/borrowproducts', function ($request, $response, $args) {
    //  $name = $_SESSION['user'];
    return $this->view->render(
        $response,
        'borrowproducts.html.twig'
    );
});
$app->post('/borrowproducts/fetch', function ($request, $response, $args) {
    $result = DB::query("SELECT * FROM books Where status='2'");
    // foreach ($result as &$item) {
    //         $item['image']  = base64_decode($item['image']);//$item->image= $xxx;
    // }
    $newArrData = array();
    foreach ($result as $key => $value) {
        $newArrData[$key] =  $result[$key];
        $newArrData[$key]['picture'] = base64_encode($result[$key]['image']);
        $newArrData[$key]['image'] = "";
    }
    return json_encode($newArrData);
});
$app->get('/category/getallcatory/[{searchTxt}]', function ($request, $response, $args) {
    $searchTxt= $args['searchTxt'];
    $statusCode = 200;
    $result = DB::query("SELECT * FROM category where name like '%$searchTxt%' and id>'5'");
    if (isset($result)) {
        $statusCode = "200";
        exit(json_encode(array("statusCode" => $statusCode, "response" => $result)));
    }
});

$app->get('/category/getmastercatory', function ($request, $response, $args) {
    $statusCode = 200;
    $result = DB::query("SELECT * FROM category where parentid = 0 ");
    if (isset($result)) {
        $statusCode = "200";
        exit(json_encode(array("statusCode" => $statusCode, "response" => $result)));
    }
});
$app->get('/category/getcatorybyid/{id}/{searchTxt}', function ($request, $response, $args) {
    $searchTxt = $args['searchTxt'];
    $id = $args['id'];
    $statusCode = 200;
    $result = DB::query("SELECT * FROM category where parentid='$id' and name like '%$searchTxt%'");
    if (isset($result)) {
        $statusCode = "200";
        exit(json_encode(array("statusCode" => $statusCode, "response" => $result)));
    }
});
?>
