<?php
//session_start(); // enable Sessions mechanism for the entire web application

require_once __DIR__ . '/vendor/autoload.php';
require_once '_setup.php';
require_once '_main.php';
require_once '_cart.php';
require_once '_users.php';


$app->run();
