<?php

require_once '_setup.php';

class ReturnInfo
{
    // Properties
    public $message;
    public $code;
    public $name;

    // // Methods
    function set_message($message)
    {
        $this->message = $message;
    }
    function set_code($code)
    {
        $this->code = $code;
    }
    function set_name($name)
    {
        $this->name = $name;
    }
}

$app->get('/login', function ($request, $response, $args) {
    return $this->view->render($response, 'login.html.twig');
});
$app->post('/login', function ($request, $response, $args) use ($log) {
    $email = $request->getParam('email');
    $password = $request->getParam('password');
    //$errorList = array();
    $result = DB::queryFirstRow("SELECT * FROM users Where email='$email'");
  
    $returnItem=new ReturnInfo();
    if (!$result) {
      
        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode(array("error" => "userNameError", "errorText" => "400 - User not found, pleas register first")));
        return $response;
    }else{
        if ($result['password'] == $password) {
            $_SESSION['userProfile'] = $result;
            if (!isset($_SESSION['cart'])) {
                $_SESSION['cart'] = array();
            }
            // $name=$_SESSION['userProfile'] ['name'];
            // $returnItem->set_name("$name");
            // $returnItem->set_code("200");
            //initial cart item
            if (isset($_SESSION['userProfile'])) {
                $id = $_SESSION['userProfile']['id'];
                $cartresult = DB::query("SELECT books.id,books.name,books.author,books.price,cart.count,cart.id as cartid
                            FROM books,cart where books.id = cart.bookid and userid = '$id' and books.status='1' ");
                $borrowcount = DB::queryFirstField("SELECT count(*) FROM borrowinfo where  userid = '$id' and (returndate is null or fine >0)");
                // $borrowresult = DB::query("SELECT books.id,books.name,books.author,borrowinfo.id as borrowid
                //             FROM books,borrowinfo right join books.id = borrowinfo.bookid and borrowinfo.userid = '$id' and books.status='1' ");

              
                foreach ($cartresult as  &$value) {
                    $item["count"] = $value["count"];
                    $item["price"] = $value["price"];
                    $item["author"] = $value["author"];;
                    $item["name"] = $value["name"];
                    $item["isnew"] =false;
                    $item["isupdate"] = false;
                    $item["cartid"] = $value["cartid"];
                    $bookid = $value["id"];
                    
                    $_SESSION['cart'][$bookid] = $item;
                }
        
            }
            $_SESSION['userProfile']['cartcount'] = sizeof($cartresult);
            $_SESSION['userProfile']['borrowcount'] = $borrowcount;
            // return json_encode($returnItem);
            $response->getBody()->write(json_encode("success-restful"));
            return $response;
        }else{
            $response = $response->withStatus(400);
            $response->getBody()->write(json_encode(array("error" => "passwordError", "errorText" => "400 - Your password is wrong, please re-input your password.")));
            return $response;
        }
    }
});

// /*****************************************register************************************************************/

$app->get('/register', function ($request, $response, $args) {
    return $this->view->render($response, 'register.html.twig');
});

// // STATE 2&3: receiving submission
$app->post('/register', function ($request, $response, $args) {
    $firstname = $request->getParam('firstname');
    $lastname = $request->getParam('lastname');
    $user = $firstname.' '. $lastname ;
    $email = $request->getParam('email');
    $pass1 = $request->getParam('password1');
    $pass2 = $request->getParam('password2');
    $phonenumber = $request->getParam('phonenumber');
    $sex = $request->getParam('sex');
    $errorList = array();
   ///^.*(?=.{6,16})(?=.*\d)(?=.*[A-Z]{2,})(?=.*[a-z]{2,})(?=.*[!@#$%^&*?\(\)]).*$/
    if (preg_match('/^[a-zA-Z0-9\ \\._\'"-]{4,50}$/', $user) != 1) { // no match
        array_push($errorList, "Name must be 4-50 characters long and consist of letters, digits, "
            . "spaces, dots, underscores, apostrophies, or minus sign.");
    }
    if (preg_match('/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/', $phonenumber) != 1) { // no match
        array_push($errorList, "Your phone must be in valid format.");
    }
    if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
        array_push($errorList, "Email does not look valid");
    } 
    if ($pass1 != $pass2) {
        array_push($errorList, "Passwords do not match");
    } else {
        $passQuality = verifyPasswordQuality($pass1);
        if ($passQuality !== TRUE) {
            array_push($errorList, $passQuality);
        }
    }
    if ($errorList) {
        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode(array("error" => "registerError", "errorText" => $errorList)));
        return $response;
    } 
    else {
        DB::insert('users', ['name' => $user, 'email' => $email, 'password' =>$pass1, 'phonenumber' => $phonenumber, 'sex'=> $sex, 'privilige'=> 2]);
        $id = DB::insertId();
        if($id){
            $result = DB::queryFirstRow("SELECT * FROM users Where id='$id'");
            $_SESSION['userProfile'] = $result;
            $_SESSION['cart'] = array();
            $_SESSION['userProfile']['cartcount']=0;
            return $response;
        }
    }
});
// /*****************************************is email taken************************************************************/
$app->get('/isemailtaken/[{email}]', function ($request, $response, $args) {
    $email = isset($args['email']) ? $args['email'] : "";
    $result  = "";
    if (!isset($args['email'])) {
        die("Error: email parameter missing");
    } else {
        $email = $args['email'];
    }
    if ($email) {
        $result = DB::queryFirstRow("SELECT * FROM users Where email='$email'");
    }
    if ($result) {
        echo "Email already in use ！";
    } else {
        echo ""; // no output if email not in use
    }
});
// /*****************************************personal page************************************************************/

$app->get('/personal', function ($request, $response, $args) {
    if (isset($_SESSION['userProfile']))
    $id= $_SESSION['userProfile']['id'];
    $userResult = DB::queryFirstRow("SELECT * FROM users Where id=$id");
    $name=$_SESSION['userProfile'] ['name'];

    return $this->view->render($response, 'personal.html.twig');
});
$app->get('/personal/mylist', function ($request, $response, $args) {
    $id = $_SESSION['userProfile']['id'];
    $result = DB::query("SELECT books.name,books.author,books.price,books.description,books.image,orderhistory.count
                            FROM orderhistory,books where books.id = orderhistory.bookid and orderhistory.userid = '$id'");
    foreach ($result as $key => $value) {
        $newArrData[$key] =  $result[$key];
        $newArrData[$key]['picture'] = base64_encode($result[$key]['image']);
        $newArrData[$key]['image'] = "";
    }
    if (isset($newArrData)) {
        return $this->view->render(
            $response,
            'mylist.html.twig',
            ["list" => $newArrData]
        );
    }else{
        return $this->view->render(
            $response,
            'mylist.html.twig'
        );
    }
   
    // return $this->view->render($response,'mylist.html.twig');
});
$app->get('/userprivilige', function ($request, $response, $args) {
    
    if (!isset($_SESSION['userProfile']) || !$_SESSION['userProfile']['privilige']){
        $response = $response->withStatus(403);
        $response->getBody()->write(json_encode(array("error" => "priviligeError", "errorText" => "Please login first")));      
    }
    if (isset($_SESSION['userProfile']['privilige'])) {
        $privilige = $_SESSION['userProfile']['privilige'];
        $statusCode = "200";
        $response = $privilige;
        return json_encode(array("statusCode" => $statusCode, "response" => $response));
    }
    return $response;
});
$container['view']->getEnvironment()->addGlobal('flashMessage', getAndClearFlashMessage());
function getAndClearFlashMessage()
{
    if (isset($_SESSION['flashMessage'])) {
        $message = $_SESSION['flashMessage'];
        unset($_SESSION['flashMessage']);
        return $message;
    }
    return "";
}
function verifyUploadedPhoto($photo, &$mime = null)
{
    if ($photo->getError() != 0) {
        return "Error uploading photo " . $photo->getError();
    }
    if ($photo->getSize() > 1024 * 1024) { // 1MB
        return "File too big. 1MB max is allowed.";
    }
    $info = getimagesize($photo->file);
    if (!$info) {
        return "File is not an image";
    }
    // echo "\n\nimage info\n";
    // print_r($info);
    // if ($info[0] < 200 || $info[0] > 1000 || $info[1] < 200 || $info[1] > 1000) {
    //     return "Width and height must be within 200-1000 pixels range";
    // }
    $ext = "";
    switch ($info['mime']) {
        case 'image/jpeg':
            $ext = "jpg";
            break;
        case 'image/gif':
            $ext = "gif";
            break;
        case 'image/png':
            $ext = "png";
            break;
        default:
            return "Only JPG, GIF and PNG file types are allowed";
    }
    if (!is_null($mime)) {
        $mime = $info['mime'];
    }
    return TRUE;
}
$app->map(['GET', 'POST'], '/personal/myinformation', function ($request, $response, array $args) {
    if ($request->getMethod() == "POST") {
        $user = $request->getParam('fullname');
        $email = $request->getParam('email');
        $pass1 = $request->getParam('password1');
        $pass2 = $request->getParam('password2');
        $phonenumber = $request->getParam('phonenumber');
        $errorList = array();
        ///^.*(?=.{6,16})(?=.*\d)(?=.*[A-Z]{2,})(?=.*[a-z]{2,})(?=.*[!@#$%^&*?\(\)]).*$/
        if (preg_match('/^[a-zA-Z0-9\ \\._\'"-]{4,50}$/', $user) != 1) { // no match
            array_push($errorList, "Name must be 4-50 characters long and consist of letters, digits, "
            . "spaces, dots, underscores, apostrophies, or minus sign.");
        }
        if (preg_match('/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/', $phonenumber) != 1) { // no match
            array_push($errorList, "Your phone must be in valid format.");
        }
        if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
            array_push($errorList, "Email does not look valid");
        }
        if ($pass1 != $pass2) {
            array_push($errorList, "Passwords do not match");
        } else {
            $passQuality = verifyPasswordQuality($pass1);
            if ($passQuality !== TRUE) {
                array_push($errorList, $passQuality);
            }
        }
        $hasPhoto = false;
        $mimeType = "";
        $uploadedImage = $request->getUploadedFiles()['image-update'];
        if ($uploadedImage->getError() != UPLOAD_ERR_NO_FILE) { // was anything uploaded?
            // print_r($uploadedImage->getError());
            $hasPhoto = true;
            $result = verifyUploadedPhoto($uploadedImage, $mimeType);
            if ($result !== TRUE) {
                $errorList[] = $result;
            }
        }
        if ($errorList) {
            //  $statusCode = "400";
            // $response = "Save failed";
            // exit(json_encode(array("statusCode" => $statusCode, "response" => $errorList)));

            $response = $response->withStatus(400);
            $response->getBody()->write(json_encode(array("error" => "changeError", "errorText" => $errorList)));
            return $response;
            
        } else {
            if ($hasPhoto) {
                $photoData = file_get_contents($uploadedImage->file);
            }
            DB::update('users', ['name' => $user, 'email' => $email, 'password' => $pass1, 
                                    'phonenumber' => $phonenumber, 'imageData' => $photoData,
                                    'imageMimeType' => $mimeType], "id=%d",$_SESSION['userProfile']['id']);
            $id= $_SESSION['userProfile']['id'];
            $result = DB::queryFirstRow("SELECT * FROM users Where id='$id'");
            $_SESSION['userProfile'] = $result;
            $statusCode = "200";
            $response = "Save success";
            exit(json_encode(array("statusCode" => $statusCode, "response" => $response)));
        }
    }
    if ($request->getMethod() == "GET") {
        // is user authenticated?
        if (($_SESSION['userProfile'])) { // refuse if user not logged in
            $id= $_SESSION['userProfile']['id'];
            $result = DB::queryFirstRow("SELECT * from users where id='$id'");
        }
        return $this->view->render($response, 'myinformation.html.twig', ['myinformation' => $result]);
    }
});
$app->get('/personal/image/{id:[0-9]+}', function ($request, $response, $args) {
  
    $user = DB::queryFirstRow("SELECT imageData,imageMimeType FROM users WHERE id=%d AND imageData IS NOT NULL", $args['id']);
    if (!$user) { // not found - FIXME
        return $response->withStatus(404);
    }
    $response->getBody()->write($user['imageData']);
    return $response->withHeader('Content-type', $user['imageMimeType']);
});

$app->get('/personal/sort',function ($request, $response) {
    //$response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    $queryParams = $request->getQueryParams(); // associative array key-values of paramaters
    $sortBy = isset($queryParams['sortBy']) ? $queryParams['sortBy'] : "id";
    // // make srue sortBy contains the name of one of todos columns
    if (!in_array($sortBy, ['name', 'email', 'phonenumber'])) {
        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode("400 - invalid sortBy value"));
        return $response;
    }
    $list = DB::query("SELECT id,email,phonenumber,name,imageMimeType FROM 
                         users where privilige='2' ORDER BY %l", $sortBy);
    return $this->view->render($response, 'accountlist.html.twig', ["list" => $list]);
   
});
function setFlashMessage($message)
{
    $_SESSION['flashMessage'] = $message;
}
// /*****************************************forgot page************************************************************/

$app->get('/passreset_request', function ( $request,  $response, $args) {
    //$view = Twig::fromRequest($request);
// return $this->view->render($response, 'login.html.twig');
    return $this->view->render($response, 'password_reset.html.twig');
});
$app->post('/passreset_request', function ( $request,  $response,$args) {
    global $log;
    //$view = Twig::fromRequest($request);
    $post = $request->getParsedBody();
    $email = filter_var($post['email'], FILTER_VALIDATE_EMAIL); // 'FALSE' will never be found anyway
    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
    if ($user) { // send email
        $secret = generateRandomString(60);
        $dateTime = gmdate("Y-m-d H:i:s"); // GMT time zone
        DB::insertUpdate('passwordresets', [
            'userId' => $user['id'],
            'secret' => $secret,
            'creationDateTime' => $dateTime
        ], [
            'secret' => $secret,
            'creationDateTime' => $dateTime
        ]);
        //
        // primitive template with string replacement
        $emailBody = file_get_contents('templates/password_reset_email.html.strsub');
        $emailBody = str_replace('EMAIL', $email, $emailBody);
        $emailBody = str_replace('SECRET', $secret, $emailBody);
       
        $config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey(
            'api-key',
            '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'
        );
        $apiInstance = new SendinBlue\Client\Api\SMTPApi(new GuzzleHttp\Client(), $config);
        $sendSmtpEmail = new SendinBlue\Client\Model\SendSmtpEmail();
        $sendSmtpEmail->setSubject("Password reset for libraryproject.ipd21.com");
        $sendSmtpEmail->setSender(new SendinBlue\Client\Model\SendSmtpEmailSender(
            ['name' => 'No-Reply', 'email' => 'noreply@libraryproject.ipd21.com']
        ));
        $sendSmtpEmail->setTo([new \SendinBlue\Client\Model\SendSmtpEmailTo(
            ['name' => $user['name'], 'email' => $email]
        )]);
        $sendSmtpEmail->setHtmlContent($emailBody);
        //
        try {
            $result = $apiInstance->sendTransacEmail($sendSmtpEmail);
            $log->debug(sprintf("Password reset sent to %s", $email));
            return $this->view->render($response, 'password_reset_sent.html.twig');
        } catch (Exception $e) {
            $log->error(sprintf("Error sending password reset email to %s\n:%s", $email, $e->getMessage()));
            return $response->withHeader("Location", "/error_internal", 403);
        }
        // end of option 2 code
    }   
    //
    return
    $this->view->render($response, 'password_reset_sent.html.twig');
});
function verifyPasswordquality($password)
{
    if (
        strlen($password) < 6 || strlen($password) > 100
        || preg_match("/[a-z]/", $password) == false
        || preg_match("/[A-Z]/", $password) == false
        || preg_match("/[0-9#@$%^&*()+=-\[\]';,.\/{}|<>?~]/", $password) == false
    ) {
        return "Password must be 6~100 characters,
            must contain at least one uppercase letter,
            one lower case letter,
            and one number or special character.";
    }else{
        return true;
    }
    
}
$app->map(['GET', 'POST'], '/passresetaction/{secret}', function ($request, $response, array $args) {
    global $log;
   // $view = Twig::fromRequest($request);
    // this needs to be done both for get and post
    $secret = $args['secret'];
    $resetRecord = DB::queryFirstRow("SELECT * FROM passwordresets WHERE secret=%s", $secret);
    if (!$resetRecord) {
        $log->debug(sprintf('password reset token not found, token=%s', $secret));
        return  $this->view->render($response, 'password_reset_action_notfound.html.twig');
    }
    // check if password reset has not expired
    $creationDT = strtotime($resetRecord['creationDateTime']); // convert to seconds since Jan 1, 1970 (UNIX time)
    $nowDT = strtotime(gmdate("Y-m-d H:i:s")); // current time GMT
    if ($nowDT - $creationDT > 60 * 60) { // expired
        DB::delete('passwordresets', 'secret=%s', $secret);
        $log->debug(sprintf('password reset token expired userid=%s, token=%s', $resetRecord['userId'], $secret));
        return
        $this->view->render($response, 'password_reset_action_notfound.html.twig');
    }
    // 
    if ($request->getMethod() == 'POST') {
        $post = $request->getParsedBody();
        $pass1 = $post['pass1'];
        $pass2 = $post['pass2'];
        $errorList = array();
        if ($pass1 != $pass2) {
            array_push($errorList, "Passwords don't match");
        } else {
            $passQuality = verifyPasswordQuality($pass1);
            if ($passQuality !== TRUE) {
                array_push($errorList, $passQuality);
            }
        }
        //
        if ($errorList) {
            return $this->view->render($response, 'password_reset_action.html.twig', ['errorList' => $errorList]);
        } else {
            DB::update('users', ['password' => $pass1], "id=%d", $resetRecord['userId']);
            DB::delete('passwordresets', 'secret=%s', $secret); // cleanup the record
            return  $this->view->render($response, 'password_reset_action_success.html.twig');
        }
    } else {
        return  $this->view->render($response, 'password_reset_action.html.twig');
    }
});



function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

$app->get('/borrowlist/borrowlistpage', function ($request, $response, $args) {
    if (isset($_SESSION['userProfile']['id'])) {
        $id = $_SESSION['userProfile']['id'];
        $result = DB::query("SELECT books.name,books.author,books.price,books.description,books.image,borrowinfo.borrowdate,borrowinfo.returndate ,borrowinfo.fine
                            FROM borrowinfo,books where books.id = borrowinfo.bookid 
                            and (borrowinfo.returndate is NULL or borrowinfo.fine > 0) and borrowinfo.userid = '$id'");
        foreach ($result as $key => $value) {
            $newArrData[$key] =  $result[$key];
            $newArrData[$key]['picture'] = base64_encode($result[$key]['image']);
            $newArrData[$key]['image'] = "";
        }
        $borrowcount = DB::queryFirstField("SELECT count(*)
                            FROM borrowinfo where  userid = '$id' and (returndate is null or fine >0)");
    }
   
    if (isset($borrowcount)&& $borrowcount>0) {
        $_SESSION['userProfile']['borrowcount'] = $borrowcount;
    }else{
        $borrowcount=0;
    }
    if (isset($newArrData)) {
        return $this->view->render(
            $response,
            'myborrowlist.html.twig',
            ["list" => $newArrData]
        );
    }else{
        return $this->view->render(
            $response,
            'myborrowlist.html.twig'
        );
    }
   
    // return $this->view->render($response,'mylist.html.twig');
});
