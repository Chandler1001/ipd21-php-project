<?php

session_start(); // enable Sessions mechanism for the entire web application

require_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;


// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler(dirname(__FILE__) . '/logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler(dirname(__FILE__) . '/logs/errors.log', Logger::ERROR));

if (strpos($_SERVER['HTTP_HOST'], "ipd21.com") !== false) {
    DB::$dbName = 'cp4976_libraryproject';
    DB::$user = 'cp4976_libraryproject';
    DB::$password = 'zqmVMaK3WWeUAWUdRI';
} else {
    DB::$dbName = 'ipd21project';
    DB::$user = 'ipd21project';
    DB::$password = 'xixilele1001'; ///Nepx6Be6F5CXzXnX skip-grant-tables cdKOHx5jgUVVYBgs
    DB::$port = '3333';
}

function db_error_handler($params)
{
    global $log;
    // log first
    $log->error("Database error: " . $params['error']);
    if (isset($params['query'])) {
        $log->error("SQL query: " . $params['query']);
    }
    // redirect
    header("Location: /internalerror");
    die;
}

// Create and configure Slim app
$config = ['settings' => [
    'addContentLengthHeader' => false,
    'displayErrorDetails' => true
]];
$app = new \Slim\App($config);

// Fetch DI Container
$container = $app->getContainer();

// Register Twig View helper
$container['view'] = function ($c) {
    $view = new \Slim\Views\Twig(dirname(__FILE__) . '/templates', [
        'cache' => dirname(__FILE__) . '/cache',
        'debug' => true, // This line should enable debug mode
    ]);
    // Instantiate and add Slim specific extension
    $router = $c->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));
    return $view;
};
$container['notFoundHandler'] = function ($container) {
    return function ($request, $response) use ($container) {
        return $container->view->render(
            $response->withStatus(404)
                ->withHeader('Content-Type', 'text/html'),
            'error404.html.twig'
        );
    };
};
// All templates will be given userSession variable
$container['view']->getEnvironment()->addGlobal('user', $_SESSION['userProfile']['name'] ?? null);
$container['view']->getEnvironment()->addGlobal('privilige', $_SESSION['userProfile']['privilige'] ?? null);


$productName = "ipd21-project";
$productID = "prod_HjtxWyr9SDoXjT";
$productPrice = 0.00;
$currency = "cad";

// Convert product price to cent 


// Stripe API configuration 
define('STRIPE_PUBLISHABLE_KEY', 'pk_test_51HA2pUIjlJ1JmMzP0SL1QgxQLc3N4ME0X3D3ZiPZvN4VbT9xOWBJdrrjbpw1Zv36Xx7KNJ7LCtxLEQKLwHDrZQOw00bJLdtrxF');  
define('STRIPE_API_KEY', 'sk_test_51HA2pUIjlJ1JmMzPfsIcCJT7REmnESO6hLGzYJbWmtL70DCD9cWdgI8lejxiGDRTimxrMoKIRd1r0DthYQtos42H00jS7xVia3');

define('STRIPE_SUCCESS_URL', 'https://libraryproject.ipd21.com/success.php');
define('STRIPE_CANCEL_URL', 'https://libraryproject.ipd21.com/cancel.html.twig');


// define('STRIPE_SUCCESS_URL', 'http://librarybooksale.ipd21:8888/success.php');
// define('STRIPE_CANCEL_URL', 'http://librarybooksale.ipd21:8888/cancel.html.twig');
