<?php
class ReturnInfo
{
    // Properties
    public $message;
    public $code;
    public $name;

    // // Methods
    function set_message($message)
    {
        $this->message = $message;
    }
    function set_code($code)
    {
        $this->code = $code;
    }
    function set_name($name)
    {
        $this->name = $name;
    }
}