slim and bootstrap study

2020/08/07 Changmin
Done today
- Start book lend system
- Update search engine for other 2 situation
    1 in book list, type word not in the category table, then show a alert.(php)
    2 in add book information,type word not in the category table will give a hint div after the input.(php)
- finish personal information update in the personl page.
- modified my js code to module way, I use 1 hour to learn how to extract my code to a module to reuse(sale and lend can use some same complict function).
  use export and import can perfectly fix these problem   
2. To do until next Scrum:
- book lend system join with the same page with sales system.


2020/08/08 Changmin
Done today
- Book lend system borrow list is done.
- Search engine for lend system and sales system are done.
    1 Add and modify php code with lend url and the most of them reuse the code after modified.(php)
    2 Update js make publicfunction reused for sale and borrow with no confict.(php)
    3 Borrow system can add to list and will alert if the book count to zero
2. To do until next Scrum:
- Administrator to manage different account.


2020/08/09 Changmin
Done today
--Set up database and update bookborrow table done.
- Book lend system borrow system is done.
    1 Borrow list cart page and php ajax call done.(php,js)
    2 Customer Account management page for adminstrator is done .(php)
    3 Borrow and return logic all done.
    4 Setup the session for the Borrow list cart.
2. To do until next Scrum:
- Test and all the part.

2020/08/11 Changmin
Done today
--Update database and update users table with image data and mimetype(before I use base64 encode).
- Personal page information update with image by teacher Gerogory's way.
    1 Update js and php files by today's ajax call in class.(php,js)
    2 User list image show use by  teacher Gerogory's way.(php,js)
    3 Add validation for image.
    4 Add sort for user management by  teacher Gerogory's way.
2. To do until next Scrum:
- Test and will add book information update page.

2020/08/12 Changmin
Done today
--Test all the function found bug.
-- Add validation
2. To do until next Scrum:
- Add restful api return code and information check.

2020/08/14 Changmin
Done today
-- Add restful  api return code and information check for user part.
-- Add validation
2. To do until next Scrum:
-- Add api return code and information check for user part for other part.
-- Add lend book fine paid part.

2020/08/15 Changmin
Done today
-- Add All restful  api return code and information check for user part.
-- Add Book Edit and update page impage upload and run successfull
-- borrow book fine system done!
-- Add Stripe for borrow book fine payment part. Challenge!!! Done successfull!
2. To do until next Scrum:
-- Still need to test and will write presentation tomorrow.

2020/08/16 Changmin
Done today
-- Search engine part update for more accurate.
-- Personal page update .
-- html page update to fix error, like modal show.
-- update php and js to show the borrow payment details
2. To do until next Scrum:
-- test and reservation start.


2020/08/17 Changmin
Done today
-- Update html.
-- fix a validation problem in user php .
