<?php
require_once '_setup.php';
$app->post('/borrow', function ($request, $response, $args) {
    $bookid = $request->getParam('bookid');
    if (isset($_SESSION['userProfile'])) {
   
        $id = intval($_SESSION['userProfile']['id']);
        $currentDate= date("Y-m-d");
        
        DB::insert('borrowinfo', ['userid' => $id, 'bookid' => $bookid, 'borrowdate' => $currentDate]);
        // DB::update('books', ['countnumber' => 'countnumber'- 1], "id=%s", $bookid);
        $true = DB::query("UPDATE books SET countnumber = countnumber - 1 where id = '$bookid'");
        $countnumber = DB::queryFirstField("SELECT countnumber from books where id = '$bookid'");
        if ($true) {
        # code...
            $statusCode = 200;
            $_SESSION['userProfile']['borrowcount']++;
            exit(json_encode(array("statusCode" => $statusCode, "response" => $_SESSION['userProfile']['borrowcount'])));
        }
        
    } 
});


$app->post('/cart', function ($request, $response, $args) {
    $bookid = $request->getParam('bookid');
    $name = $request->getParam('name');
    $price = $request->getParam('price');
    $author = $request->getParam('author');
    $count = $request->getParam('count');

    
    if (isset($_SESSION['userProfile'])) {
        if((isset($_SESSION['cart']))){
            if (!array_key_exists($bookid, $_SESSION['cart'])) {
                $id = intval($_SESSION['userProfile']['id']);
                //1、array_key_exists($pid,$arr)判断$arr中是否存在键值为$pid的一个一维数组，如果存在的话，就说明此商品以前购买过，只需要把数量加1
                DB::insert('cart', ['userid' => $id, 'bookid' => $bookid, 'count' => $count]);
                $cardid = DB::insertId();
                $item["count"] = $count; 
                $item["price"] = $price;
                $item["author"] = $author;
                $item["name"] = $name;
                $item["cartid"] = $cardid;
                //$item["isnew"] = true;
                $item["isupdate"] = false;
                $_SESSION['cart'][$bookid] = $item;
                $statusCode = "200";
                $response = $name." added successful!";
                exit(json_encode(array("statusCode" => $statusCode, "response" => $response)));
            } else{
                $statusCode = "";
                $response = $response->withStatus(400);
                $response->getBody()->write(json_encode(array("error" => "addCartError", "errorText" => "400 - This item is already added in your cart. Please kindly check your cart.")));
                return $response;
            }
        } 
    } else{
        $response = $response->withStatus(403);
        $response->getBody()->write(json_encode(array("error" => "priviligeError", "errorText" => "Please login first")));
        return $response;
    }
});



$app->get('/cart/cartpersonal', function ($request, $response, $args)  {
    
    if(isset($_SESSION['cart'])){ 
        $statusCode = "200";
        exit(json_encode(array("statusCode" => $statusCode, "response" => $_SESSION['cart'])));
    }
}); //cart.html.twig
$app->get('/cart/cartpersonalpage', function ($request, $response, $args) {
    return $this->view->render($response, 'cart.html.twig');
});
//delete
$app->get('/cart/delete/{id}', function ($request, $response, $args) {
    $id= $args['id'];
    foreach ($_SESSION['cart'] as $key => &$item) {
      
        if (($key == $id)) {
            $cardid= $item["cartid"];
            DB::delete('cart', 'id=%s', $cardid);
            unset($_SESSION['cart'][$id]);
            if (isset($_SESSION['userProfile']['cartcount']) && ($_SESSION['userProfile']['cartcount'] > 0)) {
                $_SESSION['userProfile']['cartcount']--;
            }
        }
    }
    $statusCode = "200";
    $response = "delete successful!";



    exit(json_encode(array("statusCode" => $statusCode, "response" => $response)));
         
        // $response = "Delete sussessful!";
        // exit(json_encode(array("statusCode" => $statusCode, "response" => $response)));
});
$app->get('/cart/update/{id}/{value}', function ($request, $response, $args) {
    $id = $args['id'];
    $count = $args['value'];
    if (($id != '') && ($count != '')) {
        if (isset($_SESSION['cart'])) {
            foreach ($_SESSION['cart'] as $key =>  &$item) {
                if (($id == $key)) {
                    $item['count'] = $count;
                    if ((isset($item["cartid"]))) {
                        DB::update('cart', ['count' => $item["count"]], "id=%s", $item["cartid"]);
                        continue;
                    } 
                    
                }
            }
            $statusCode = "200";
            $response = "Update successful!";



            exit(json_encode(array("statusCode" => $statusCode, "response" => $response)));
        }
    }

});
//$paymenttype;
$app->get('/bill/billlist', function ($request, $response, $args) {
    //global $paymenttype;
    //global $paymenttype;
    $queryParams = $request->getQueryParams();
    $_SESSION['paymenttype'] = $queryParams['paymenttype'];
    return $this->view->render($response, 'billlist.html.twig');
});
$app->get('/bill/billDetails', function ($request, $response, $args){
    $id = $_SESSION['userProfile']['id'];
    //global $paymenttype;
    $paymenttype= $_SESSION['paymenttype'];
    $billlist = array();
    $result = DB::query("SELECT id, email,item_price_currency,paid_amount,	payment_status,modified
                        FROM orders where userid = '$id' and paymenttype='$paymenttype'");
    foreach ($result as  &$value) {
        $item["email"] = $value["email"];
        $item["paid_amount"] = $value["paid_amount"];
        $item["item_price_currency"] = $value["item_price_currency"];;
        $item["payment_status"] = $value["payment_status"];
        $item["modified"] = $value["modified"];
        $billid = $value["id"];
        $billlist[$billid] = $item;
    }
   
    if (isset($billlist)) {
        $statusCode = "200";
        exit(json_encode(array("statusCode" => $statusCode, "response" => $billlist)));
    }
});
$app->get('/cart/bill/[{id}]', function ($request, $response, $args) {
    $id= $args['id'];
    $result = DB::query("SELECT books.name,books.price,orderhistory.count,orderhistory.totalprice
                            FROM orderhistory,books where books.id = orderhistory.bookid and orderhistory.orderid = '$id'");

    return $this->view->render(
        $response,
        'bill.html.twig',
        ['list' => $result]
    );
});
// $app->get('/cart/billdetails', function ($request, $response, $args) {
//     return $this->view->render($response, 'bill.html.twig');
// });
$app->get('/cart/purchase', function ($request, $response, $args) {
    return $this->view->render($response, 'purchase.html.twig');
});
$app->get('/cart/{action}', function ($request, $response, $args) {
    $action = isset($args['action']) ? $args['action'] : "";
    if (isset($_SESSION['userProfile']['cartcount'])) {
        $statusCode = "200";
        
        if ($action== 'getcount') {
            exit(json_encode(array("statusCode" => $statusCode, "response" => $_SESSION['userProfile']['cartcount'])));
        } else if ($action == 'add') {
           $_SESSION['userProfile']['cartcount'] ++;
            exit(json_encode(array("statusCode" => $statusCode, "response" => $_SESSION['userProfile']['cartcount'])));
        } 
    }else{
        $statusCode = "200";
        exit(json_encode(array("statusCode" => $statusCode, "response" => 0)));
    }
});
$app->get('/borrow/getcount', function ($request, $response, $args) {
    if (isset($_SESSION['userProfile']['borrowcount'])) {
        $statusCode = "200";
         exit(json_encode(array("statusCode" => $statusCode, "response" => $_SESSION['userProfile']['borrowcount'])));
       } else {
        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode(array("error" => "borrowError", "errorText" => "400 - borrowError.")));
        return $response;
    }
});

///borrowlist/borrowlistpage