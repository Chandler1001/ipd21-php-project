<?php
require_once 'vendor/autoload.php';
$config = [
    'callback' => 'https://libraryproject.ipd21.com/google.php',//http://localhost:8888/google.php
    'keys'     => [
        'id' => '892313469817-llsuhs6lkqi4d8fb8gg6kf1vjtmhnlpg.apps.googleusercontent.com',
        'secret' => '9lXKXv8eJ8le0JsU-UJ90IxX'
    ],
    'scope'    => 'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email',
    'authorize_url_parameters' => [
        'approval_prompt' => 'force', // to pass only when you need to acquire a new refresh token.
        'access_type' => 'offline'
    ]
];

$adapter = new Hybridauth\Provider\Google($config);
